/*
  Filename : popup.js
  Description : Javascript to load latest posts from my favorite sites. 
*/
function load_content(url,title,table_id,td_id,a_id) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
      var resp = xhr.responseText;
      var r = document.getElementById('resp2');
      r.innerHTML = resp; // for DOM access
      var a = r.getElementsByTagName("table")[table_id].getElementsByTagName("td")[td_id].getElementsByTagName("a")[a_id];
      if(a!=null && a.innerHTML.length>0) {
        document.getElementById("resp").innerHTML += "<h2>" + title + "</h2>" + "<a target='_new' href='" + a + "'>" + a + "</a><br/><hr/><br/>";
      } else {
				document.getElementById("resp").innerHTML += "<h2>" + title + "</h2>" + "New episode coming out soon<br/><hr/><br/>";
      }
      document.getElementById("resp2").innerHTML = ""; // clear out
    };

  }
  xhr.send();
}

window.onload = function() {
  load_content('http://www.narutoget.com/cat/18-narutosd-rock-lee-and-his-ninja-pals/',"Naruto SD",0,0,0);
  load_content('http://www.narutoget.com/', "Naruto",0,0,0);
  load_content('http://www.fairytail.tv/1/',"Fairy Tail",0,0,0);
  load_content('http://www.mangahit.com/manga/naruto',"Naruto Manga",3,3,0);
  load_content('http://www.mangahit.com/manga/fairy-tail',"Fairy Tail Manga",3,3,0);

};
